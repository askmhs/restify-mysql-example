import restify from "restify";
import DBConnection from "./config/database";

/**
 * Creating server
 */
const server = restify.createServer({
	name: "restify-mysql"
});

/**
 * Configure server parser
 * This will allow server to parse request body and query
 */
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

/**
 * Sync database connection
 */
DBConnection.sync({
	force: false // This will refresh the database tables and structures
}).then(() => {
	console.log("Schema synced successfully!");

	/**
     * Call router
     */
	require("./src/routes")(server);
}).catch(error => {
	console.log("An error occurred while syncing schema");
	throw error;
});

/**
 * Starting server
 */
server.listen(3000, () => {
	console.log("Server up on port 3000");
});