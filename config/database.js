import Sequelize from "sequelize";

/**
 * Load ENV file
 */
require("dotenv").config();

let DB_URI = `${process.env.DB_DRIVER}://${process.env.DB_USERNAME}@${process.env.DB_HOST}/${process.env.DB_DATABASE}`;

/**
 * If password is filled
 */
if (process.env.DB_PASSWORD.length > 0) {
	DB_URI = `${process.env.DB_DRIVER}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_DATABASE}`;
}

/**
 * Create new connection
 */
const connection = new Sequelize(DB_URI);

/**
 * Attempt to connect
 */
connection.authenticate().then(() => {
	console.log("Database connection established successfully");
}).catch(error => {
	console.log("An error occurred while establishing database connection");
	throw error;
});

/**
 * Import schema
 */
connection.UserModel = connection.import("./../src/model/user-model");

module.exports = connection;