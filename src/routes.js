import UserController from "./controllers/UserController";

module.exports = server => {

	server.get("/", (req, res) => {
		res.json("Hello, world!");
	});

	/**
     * User routes
     */
	server.get("/user", UserController.index);
	server.post("/user", UserController.store);
	server.put("/user/:id", UserController.update);
	server.del("/user/:id", UserController.destroy);
};