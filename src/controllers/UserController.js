import {
	UserModel
} from "./../../config/database";

const userController = {};

/**
 * Get list users
 */
userController.index = async (req, res) => {
	const users = await UserModel.findAll({
		raw: true,
		paranoid: false // Include trashed data
	});

	res.json(users);
};

/**
 * Create user
 */
userController.store = async (req, res) => {
	const created = await UserModel.create(req.body);
	res.json(created);
};

/**
 * Update user
 */
userController.update = async (req, res) => {
	/**
     * Find user
     */
	const user = await UserModel.findByPk(req.params.id);

	/**
     * Validate user
     */
	if (user !== null) {
		/**
         * Update user
         */
		await user.update(req.body);
		res.json(user);
	} else {
		res.status(404);
		res.json("User data not found!");
	}
};

/**
 * Delete user
 */
userController.destroy = async (req, res) => {
	/**
     * Find user
     */
	const user = await UserModel.findByPk(req.params.id);

	/**
     * Validate user
     */
	if (user !== null) {
		/**
         * Update user
         */
		await user.destroy(req.body);
		res.json(user);
	} else {
		res.status(404);
		res.json("User data not found!");
	}
};

module.exports = userController;