import Sequelize from "sequelize";

export default connection => {
	/**
     * Define table
     */
	return connection.define("users", {
		/**
         * Define columns
         */
		id: {
			type: Sequelize.DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: Sequelize.DataTypes.STRING,
			allowNull: false
		}
	}, {
		createdAt: "createdAt",
		updatedAt: "updatedAt",
		paranoid: true // enable soft delete
	});
};